﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrockMsg
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        List<Label> nameLabels; // List to keep track of labels that will show Name IDs.
        List<string> names;
        BrokNameManager bnm;
        Thread svrThread;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            nameLabels = new List<Label>();

            new Thread(new ThreadStart(() =>
            {
                bnm = new BrokNameManager();
                bnm.StoreList();
                this.Dispatcher.Invoke(() => RefreshNameList());
            })).Start() ;
            

            //Start listening
            svrThread = new Thread(new ThreadStart(() => {
                BrokServerModule svr = new BrokServerModule();
            }));
            svrThread.Start();
        }

        private void RefreshNameList()
        {
            names = new BrokNameManager(true).GetAllNames();
            if (nameLabels.Count > 0)
            {
                // If labels were drawn previously, erase them and empty the list.
                foreach (Label nl in nameLabels)
                {
                    ContactsPanel.Children.Remove(nl);
                }
                nameLabels = new List<Label>();
            }

            //Draw labels with Name IDs
            int i = 0;
            foreach (string name in names)
            {
                Label l = new Label();
                
                l.Content = name;
                l.MouseEnter += NameId_Hover;
                l.MouseLeave += NameId_Leave;
                l.MouseDown += NameID_Click;
                l.FontFamily = new FontFamily("Arial");
                l.FontSize = 12;
                l.Margin = new Thickness(3);
                
                Canvas.SetTop(l, 30 * i + 25);
                Canvas.SetLeft(l, 35);

                nameLabels.Add(l); //Add it to the list to keep track of the label.
                
                ContactsPanel.Children.Add(l);
                i++;
            }
        }


        private void NameId_Hover(object sender, EventArgs e)
        {
            Label l = (Label)sender;

            l.BorderThickness = new Thickness(1);
            l.BorderBrush = Brushes.Black;
        }

        private void NameId_Leave(object sender, EventArgs e)
        {
            Label l = (Label)sender;

            l.BorderThickness = new Thickness(0);
            l.BorderBrush = null;
        }

        private void NameID_Click(object sender, EventArgs e)
        {
            Label lb = (Label)sender;
            ConvWindow conv = new ConvWindow();
            conv.username = (string)lb.Content;
            conv.hostname = bnm.GetHost((string)lb.Content);
            conv.Show();

        }

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            svrThread.Abort();
        }
    }
}
