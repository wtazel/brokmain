﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;

namespace BrockMsg
{
    /// <summary>
    /// Interaction logic for ConvWindow.xaml
    /// </summary>
    public partial class ConvWindow : Window
    {
        Button sendButton;
        public string username;
        public string hostname;

        public ConvWindow()
        {
            InitializeComponent();
        }

        private void sendButton_Click()
        {
            if (Sendable())
            {
                string msg = PrepSendMessage(WriteBox.Text);
                SendMessage(msg);
            }
        }

        private string PrepSendMessage(string data)
        {
            return data.Trim();
        }

        private void SendMessage(string data)
        {
            PrintSentData(data);

            new Thread(new ThreadStart(() =>
            {
                new BrokClientModule(hostname, username, data);
            })).Start();
            

            WriteBox.Text = String.Empty;
        }

        private bool Sendable()
        {
            string s = WriteBox.Text.Trim();

            if (s.Length <= 0 || s == String.Empty)
            {
                return false;
            }
            
            return true;
        }


        public void PrintRecvData(string dat)
        {
            Label l = new Label();
            l.Padding = new Thickness(7);

            l.FontSize = 11;
            l.Content = username + ":  " + dat;
            this.Dispatcher.Invoke(() => { MessagesBox.Children.Add(l); }); //Need to use Invoke because this method is called from the Server object which usually runs on a separate thread.

            ScrollMessagesBox.ScrollToEnd();
        }

        public void PrintSentData(string dat)
        {
            Label l = new Label();
            
            l.Padding = new Thickness(7);
            l.FontSize = 11;

            l.Content = "You:  " + dat;
            
            MessagesBox.Children.Add(l);
            ScrollMessagesBox.ScrollToEnd();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            sendButton = new Button();
            //sendButton.MouseDown += sendButton_Click;

            this.Title = username + " - Brok [" + hostname + "]";

        }

        private void WriteBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                sendButton_Click();
            }
        }
    }
}
