﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace BrockMsg
{
    class BrokNameManager
    {
        private string unsHost = "https://taselifiedtv.000webhostapp.com/list.txt";

        //private Dictionary<string, string> namemap;

        private string namelist;
        private bool retErr;
        private string appPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "//Brok";
        private string listFile = "ns.list";

        public string GetHost(string name)
        {
            string p = appPath + "//" + listFile;
            if (File.Exists(p))
            {
                StreamReader sr;
                try
                {
                    sr = new StreamReader(p);
                    while (true)
                    {
                        string search = sr.ReadLine();
                        if (search == null) break;

                        string[] res = search.Split(' ');

                        if (res[0] == name)
                        {
                            sr.Close();
                            return res[1];
                        }

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error while getting host " + e.Message);
                    return null;
                }

                sr.Close();
            }
            return null;
        }

        public List<string> GetAllNames()
        {
            string p = appPath + "//" + listFile;
            List<string> names = new List<string>();

            if (File.Exists(p))
            {
                StreamReader sr;
                try
                {
                    sr = new StreamReader(p);
                    while (true)
                    {
                        string search = sr.ReadLine();
                        if (search == null) break;

                        string[] res = search.Split(' ');
                        names.Add(res[0]);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Error while getting host " + e.Message);
                    return null;
                }

                sr.Close();
            }
            return names;
        }



        public BrokNameManager(bool skipRetr = false)
        {
            if (!skipRetr)
                RetrieveNameList();
        }

        public void Retry()
        {
            RetrieveNameList();
        }

        public void StoreList()
        {
            if (!Directory.Exists(appPath))
            {
                Console.WriteLine("Application directory not found.");
                try
                {
                    Directory.CreateDirectory(appPath);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to create directory! " + e.Message);
                    return;
                }
            }

            string p = appPath + "//" + listFile;
            try
            {
                StreamWriter sw = new StreamWriter(p);
                sw.WriteLine(namelist);
                sw.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to write name list! " + e.Message);
                return;
            }

        }

        private void RetrieveNameList()
        {
            retErr = false;
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(unsHost);

            try
            {
                using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
                using (Stream s = res.GetResponseStream())
                using (StreamReader sr = new StreamReader(s))
                {
                    namelist = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                namelist = String.Empty;
                retErr = true;
            }


        }

        public string Namelist
        {
            get { return this.namelist; }
        }

        public bool RetErr
        {
            get { return this.retErr; }
        }

    }
}
