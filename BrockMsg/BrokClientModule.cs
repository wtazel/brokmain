﻿using System;
using System.Net.Sockets;
using System.Text;

namespace BrockMsg
{
    class BrokClientModule
    {
        public TcpClient client;

        public BrokClientModule(string targetHostname, string senderName, string data)
        {
            try
            {
                client = new TcpClient(targetHostname, 5678);
                NetworkStream stream = client.GetStream();
                SendData(stream, senderName); // Send the client name first

                string resp = RecvData(stream); //Wait for the confirmation

                SendData(stream, data);
                client.Close();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }

        }
        private void SendData(NetworkStream ns, string dat)
        {
            Byte[] data = Encoding.ASCII.GetBytes(dat);
            ns.Write(data, 0, data.Length);

        }

        private String RecvData(NetworkStream ns)
        {
            Byte[] data = new Byte[1024];
            String resp = String.Empty;

            int k = ns.Read(data, 0, data.Length);
            resp = Encoding.ASCII.GetString(data, 0, k);

            return resp;
        }
    }
}
