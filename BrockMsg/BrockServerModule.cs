﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Windows;
using System.Linq;

namespace BrockMsg
{
    public class BrokServerModule
    {
        TcpListener listener;

        string clientName;

        public BrokServerModule()
        {
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 5678);
                listener.Start();

                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();

                    //At this point, a connection has been established 
                    NetworkStream stream = client.GetStream();

                    string resp = RecvData(stream);
                    clientName = resp; // The name of the client is received first.

                    SendData(stream, "!ack!"); //Send a message to confirm that the clientName has been received

                    //Data will be received from the client.
                    resp = RecvData(stream);

                    //Open a conversation window, unless it's already open.
                    bool alreadyOpen = false;
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        WindowCollection fc = Application.Current.Windows;
                        foreach (Window frm in fc)
                        {
                            if (frm is ConvWindow)
                            {
                                ConvWindow cfrm = (ConvWindow)frm;
                                if (cfrm.username == clientName)
                                {
                                    Console.WriteLine("ConvWindow already open.");
                                    alreadyOpen = true;
                                    cfrm.PrintRecvData(resp);
                                }
                            }

                        }
                    });

                    if (!alreadyOpen)
                    {
                        ConvWindow cv = new ConvWindow();
                        cv.username = clientName;
                        cv.hostname = new BrokNameManager(true).GetHost(clientName);
                        cv.PrintRecvData(resp);
                        cv.Show();
                    }

                    client.Close();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Warning! An exception has occured and the server has stopped listening!");
                Console.WriteLine("Exception: " + e.Message);
                Console.WriteLine(e.StackTrace);
            }

        }

        private void SendData(NetworkStream ns, string dat)
        {
            Byte[] data = Encoding.ASCII.GetBytes(dat);
            ns.Write(data, 0, data.Length);

        }

        private String RecvData(NetworkStream ns)
        {
            Byte[] data = new Byte[1024];
            String resp = String.Empty;

            int k = ns.Read(data, 0, data.Length);
            resp = Encoding.ASCII.GetString(data, 0, k);

            return resp;
        }

        public string ClientName
        {
            get { return this.clientName; }
        }

        private bool IsWindowOpen<T>(string name = "") where T : Window
        {
            return string.IsNullOrEmpty(name)
               ? Application.Current.Windows.OfType<T>().Any()
               : Application.Current.Windows.OfType<T>().Any(w => w.Name.Equals(name));
        }
    }
}
